<?php defined('BASEPATH') OR exit('No direct script access allowed') ?>

<title><?= isset($page_title) ? $page_title . ' | ' : '' ?> <?= __session('school_name') ?></title>
<meta charset="utf-8" />
<meta name="apple-mobile-web-app-status-bar-style" content="#002447">
<meta name="theme-color" content="#002447">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="<?= __session('meta_keywords') ?>"/>
<meta name="description" content="<?= __session('meta_description') ?>"/>
<meta name="subject" content="Situs Pendidikan">
<meta name="copyright" content="<?= __session('school_name') ?>">
<meta name="language" content="Indonesia">
<meta name="robots" content="index,follow" />
<meta name="revised" content="Sunday, July 18th, 2010, 5:15 pm" />
<meta name="Classification" content="Education">
<meta name="author" content="Anton Sofyan, 4ntonsofyan@gmail.com">
<meta name="designer" content="Anton Sofyan, 4ntonsofyan@gmail.com">
<meta name="theme:name" content="Barka">
<meta name="theme:author" content="Diki Siswanto">
<meta name="theme:version" content="<?= THEME_VERSION ?>">
<meta name="reply-to" content="4ntonsofyan@gmail.com">
<meta name="owner" content="Anton Sofyan">
<meta name="url" content="http://www.sekolahku.web.id">
<meta name="identifier-URL" content="http://www.sekolahku.web.id">
<meta name="category" content="Admission, Education">
<meta name="coverage" content="Worldwide">
<meta name="distribution" content="Global">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Copyright" content="<?= __session('school_name') ?>" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="revisit-after" content="7" />
<meta name="webcrawlers" content="all" />
<meta name="rating" content="general" />
<meta name="spiders" content="all" />
<meta itemprop="name" content="<?= __session('school_name') ?>" />
<meta itemprop="description" content="<?= __session('meta_description') ?>" />
<meta itemprop="image" content="<?= base_url('media_library/images/'. __session('logo')) ?>" />
<meta name="csrf-token" content="<?= __session('csrf_token') ?>">
<meta name="google-adsense-account" content="ca-pub-2746545953256524">
<?php if (isset($post_type) && $post_type == 'post') : ?>
  
  <meta property="og:url" content="<?= current_url() ?>" />
  <meta property="og:type" content="article" />
  <!--<meta property="og:image:width" content="1600" />-->
  <!--<meta property="og:image:height" content="1280" />-->
  <meta property="og:title" content="<?= $query->post_title ?>" />
  <meta property="og:description" content="<?= word_limiter(strip_tags($query->post_content), 30) ?>" />
  <meta property="og:image" content="<?= base_url('media_library/posts/large/'.$query->post_image) ?>" />
  <meta property="og:site_name" content="<?= __session('school_name') ?>" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:site" content="@smkikakartika" />
  <meta name="twitter:creator" content="@smkikakartika">
  <meta name="twitter:title" content="<?= $query->post_title ?>" />
  <meta name="twitter:description" content="<?= word_limiter(strip_tags($query->post_content), 30) ?>" />
  <meta name="twitter:image" content="<?= base_url('media_library/posts/large/'.$query->post_image) ?>" />
  
  <link rel="canonical" href="<?= current_url() ?>" />
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
  <link rel="alternate" type="application/rss+xml" title="<?= __session('school_name') ?> &raquo; Feed" href="<?= base_url() ?>feed/" />
  <link rel="alternate" type="application/rss+xml" title="<?= __session('school_name') ?> &raquo; <?= $query->post_title ?>" href="<?= current_url() ?>/feed/" />
<?php endif ?>
<link rel="icon" href="<?= base_url('media_library/images/'.__session('favicon')) ?>">
<link rel="alternate" type="application/rss+xml" title="<?= __session('school_name') ?> Feed" href="<?= base_url('feed') ?>" />